# River Raid Runner

This is a runner-style game inpired on 1982's game River Raid designed by Carol Shaw/Activision for the Atari 2600.

<div align="center">
  <img src="https://drive.google.com/uc?id=1WSkrzEik8P6D9xXl5N8RUC_KTM5eSyj3&export=download" width="360" height="640">
</div>

A modern smartphone device is very different from a 80's videogame: screen is rectangular instead of square, much smaller than a TV and touch screen controls cannot provide precise and fast responses to play the original version.

So this game try to adapt the concept to the device with the following features: 
* Portrait screen wich is a very good choice for a vertical shooter;
* Runner concept, wich fits pretty well in this design;
* Swipe controls wich is more adequate for touch-screen devices.

You can download the Android app in the Google Play Store (Renamed Retro Warplane): https://play.google.com/store/apps/details?id=com.robles8bit.retrowarplane

## Features

* Four enemy types;
* Fuel panel;
* Fuel tanks;
* Bridges;
* Random generated terrain.

## Architecture

* The game is written in pure JavaScript, without using any frameworks or even jQuery;
* The animation runs on a canvas at 60 FPS;
* The resolution of 160x284 was chosen to emulate a "portrait" 80's video game.
